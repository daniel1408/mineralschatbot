﻿using BingSearchImageSDK.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BingSearchImageSDK.Services
{
    public static class BingImageSearch
    {
        const string accessKey = "6110ea988c0740a0ac47d670b3a2e363";
        public static async Task<string> GetContentUrl(string locale, string text)
        {
            text = "mineral " + text;
            string uriBase = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";
            HttpResponseMessage response = await MakeHttpGetRequest(text, locale);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            string responseString = await response.Content.ReadAsStringAsync();


            ImageSearchModel responseJson = JsonConvert.DeserializeObject<ImageSearchModel>(responseString);
            try
            {
                int index = 0;
                if (responseJson.value.Length > 3)
                {
                    Random rnd = new Random();
                    index = rnd.Next(0, 3);
                }
                var responseFirstValue = responseJson.value[index];
                return responseFirstValue.contentUrl;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static async Task<HttpResponseMessage>
            MakeHttpGetRequest(string searchQuery, string locale)
        {

            var urlB ="https://api.cognitive.microsoft.com/bing/v7.0/images/search";
            string uriQuery = urlB + "?q=" + Uri.EscapeDataString(searchQuery) +
                "&size=large";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", accessKey);
                HttpResponseMessage response = await client.GetAsync(uriQuery);
                return response;
            }




        }
    }
}

