
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace segchatbot.Models
{
    [Serializable]
    public class RequestData
    {
        public Mineral mineral { get; set; }
        public int sequence { get; set; }
    }
}