﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;

namespace segchatbot.Models
{
    [Serializable]
    public class TraceColor
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}