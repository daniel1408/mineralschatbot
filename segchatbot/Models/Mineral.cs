
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace segchatbot.Models
{
    [Serializable]
    public class Mineral
    {
        public int id { get; set; }
        public string name { get; set; }
        public float density {get; set;}
        public string densityText {get; set;}
        public float toughness { get; set; }
        public string description {get; set;}
        public bool isMetalic {get; set;}
        public bool isLiquid {get; set;}
        public bool isFuse {get; set;}
        public bool isMagnetic { get; set; }
        public bool reageHcl { get; set; }
        public string mainColor {get; set;}
        public string mainTrace {get; set;}
        public ICollection<TraceColor> traceColorList { get; set; }
        public ICollection<Color> colorList { get; set; }
    }
}