﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace segchatbot.Dialogs
{
    [Serializable]
    public class DensityDialog : IDialog<string>
    {
        //Mineral mineral;
        //string low;
        //string higt;
        //string middle;

        //public DensityDialog(Mineral mineral)
        //{
        //    this.mineral = mineral;
        //    if(this.mineral.isMetalic)
        //    {
        //        low = "0;4.5";
        //        middle = "4.5;5.5";
        //        higt = "5.5;20";
        //    } else
        //    {
        //        low = "0;2.5";
        //        middle = "2.5;3";
        //        higt = "3;20";
        //    }
        //}
        public async Task StartAsync(IDialogContext context)
        {
            SuggestedActions suggest = new SuggestedActions();
            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Baixa",
                Value = "baixa",
                Type = "imBack"
            });

            actions.Add(new CardAction()
            {
                Title = "Média",
                Value = "média",
                Type = "imBack"
            });

            actions.Add(new CardAction()
            {
                Title = "Alta",
                Value = "alta",
                Type = "imBack"
            });

            suggest.Actions = actions;
            var reply = context.MakeMessage();

            reply.Text = "Lembrando que para minerais não metálicos a densidade média está entre 2,5 g/cm³ e 3,5 g/cm³ e para minerais metálicos está entre 3,5 g/cm³ e 3 g/cm³.";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = suggest;

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;
            context.Done(message.Text);
        }
    }
}