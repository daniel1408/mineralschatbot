﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace segchatbot.Dialogs
{
    [Serializable]
    public class TraceColorDialog : IDialog<string>
    {
        readonly List<Mineral> mineralList;
        public TraceColorDialog(List<Mineral> mineralList)
        {
            this.mineralList = mineralList;
        }

        public async Task StartAsync(IDialogContext context)
        {
            SuggestedActions suggest = new SuggestedActions();
            var actions = new List<CardAction>();

            List<string> traceColorTemp = new List<string>();
            foreach (Mineral element in mineralList)
            {
                foreach (TraceColor traceColor in element.traceColorList)
                {
                    if (traceColorTemp.IndexOf(traceColor.name) < 0)
                    {
                        actions.Add(new CardAction()
                        {
                            Title = traceColor.name,
                            Value = traceColor.name,
                            Type = "imBack"
                        });
                        traceColorTemp.Add(traceColor.name);
                    }
                }
            }

            suggest.Actions = actions;
            var reply = context.MakeMessage();

            reply.Text = "Escolha alguma das cores que melhor represente a cor predominante do traço do seu mineral";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = suggest;

            await context.PostAsync(reply);

            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;
            context.Done(message.Text);
        }
    }
}