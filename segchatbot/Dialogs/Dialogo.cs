﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using segchatbot.Dialogs;
using segchatbot.Models;
using segchatbot.Service;
using segchatbot.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace segchatbot
{
    [Serializable]
    [LuisModel("5766281e-bbc6-49d3-a0ab-19467238090d", "0b9d35219fee43e3b9571d43b27288dd")]
    public class Dialogo : LuisDialog<Object>
    {
        private Mineral mineralTemp;
        private List<Mineral> localMineralList;

        [LuisIntent("greeting")]
        public async Task Saudacao(IDialogContext context, LuisResult result)
        {
            string userSaudacao;
            string nome = context.Activity.From.Name.Split(' ')[0] == "username" ? "Usuário(a)" : context.Activity.From.Name.Split(' ')[0];
            
            if (result.Query.Contains("bom dia"))
                userSaudacao = $"Bom dia {nome}, tudo bem?";
            else
            if (result.Query.Contains("boa tarde"))
                userSaudacao = $"Boa tarde {nome}, tudo bem?";
            else
            if (result.Query.Contains("boa noite"))
                userSaudacao = $"Boa noite {nome}, tudo bem?";
            else
                userSaudacao = $"Olá {nome}!";

            await printOptions(context, userSaudacao);
        }

        private async Task printOptions(IDialogContext context, string saudacao)
        {
            await context.PostAsync(saudacao + " Sou o Tungstênio, seu chatbot mineralogista.");
            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Menu de Opções",
                Value = "menu de opções",
                Type = "imBack"
            });

            var text = "Veja meu menu de opções ou me diga em que posso lhe ajudar";
            await context.PostAsync(await SuggestAction(context, text, actions));
        }

        [LuisIntent("Menu")]
        public async Task Menu(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Vc pode escolher alguma das opções do menu ou pode digitar o que deseja que eu lhe ajude.");
            await Delay();

            IMessageActivity menuMessage = context.MakeMessage();
            menuMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            menuMessage.Attachments = await GetAttachment.Menu();
            await context.PostAsync(menuMessage);
        }

        [LuisIntent("goodbye")]
        public async Task Despedida(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Até mais, volte sempre. ;-).");
        }

        [LuisIntent("help")]
        public async Task Help(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Conversando comigo pode navegar pelos menús ou digitar o que deseja a qualquer momento, por exemplo: \"quero saber sobre grafita\" ou \"identificar mineral\".");
            await Delay();
            await context.PostAsync("Caso eu não entenda sua frase, tente formulá-la de maneira mais clara e simples para que eu lhe forneça a melhor resposta possível. ;-).");
            await Delay();
            await context.PostAsync(await About(context));
        }

        private static async Task<IMessageActivity> About(IDialogContext context)
        {
            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Menu principal",
                Value = "Menu principal",
                Type = "imBack"
            });

            actions.Add(new CardAction()
            {
                Title = "Sobre o Sistema",
                Value = "https://minerals.azurewebsites.net/Home/About",
                Type = "openUrl"
            });

            var text = "Para saber mais, vc pode acessar nossa página que descreve o que é um chatbot e meu funcionamento de maneira mais detalhanda...";
            return await SuggestAction(context, text, actions);
        }

        [LuisIntent("None")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Não entendi muito bem o que vc disse :-(. tente melhorar sua frase e tente novamente.");
            await Delay();
            await context.PostAsync(await BackMenu(context));
        }

        [LuisIntent("About-minerals")]
        public async Task AboutMinerals(IDialogContext context, LuisResult result)
        {
            if (result.Entities.Count == 0)
            {
                await context.PostAsync("Temos aqui alguns exemplos de minerais.");
                await Delay();
                var actions = new List<CardAction>();
                actions.Add(new CardAction()
                {
                    Title = "Grafita",
                    Value = "Saber sobre grafita",
                    Type = "imBack"
                });

                actions.Add(new CardAction()
                {
                    Title = "Ouro",
                    Value = "Saber sobre ouro",
                    Type = "imBack"
                });

                actions.Add(new CardAction()
                {
                    Title = "Galena",
                    Value = "Saber sobre galena",
                    Type = "imBack"
                });

                actions.Add(new CardAction()
                {
                    Title = "Magnetita",
                    Value = "Saber sobre magnetita",
                    Type = "imBack"
                });

                actions.Add(new CardAction()
                {
                    Title = "Prata",
                    Value = "Saber sobre prata",
                    Type = "imBack"
                });
                var text = "Mas vc tbm pode digitar algo do tipo: \"saber sobre diamante\", \"quero informações da azurita\".";
                await context.PostAsync(await SuggestAction(context, text, actions));
            }
            else
            {
                string entity = result.Entities[0].Entity;
                Mineral mineral = null;
                try
                {
                    string textEncode = System.Web.HttpUtility.UrlEncode(entity, Encoding.GetEncoding("iso-8859-7"));
                    mineral = MineralsApi.getMineralByName(textEncode);

                    if (mineral == null)
                    {
                        await context.PostAsync("Infelizmente não achei o mineral com essas características. Irei atualizar minha base para lhe atender.");
                        await Delay();
                        await context.PostAsync(await BackMenu(context));
                    }
                    else
                    {
                        IMessageActivity menuMessage = context.MakeMessage();
                        menuMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                        menuMessage.Attachments = await GetAttachment.GetMineralInformation(mineral);

                        await context.PostAsync(menuMessage);
                        await context.PostAsync(await BackMenu(context));
                    }
                }
                catch (Exception)
                {
                    await context.PostAsync(await Error(context));
                    await context.PostAsync(await BackMenu(context));
                }
            }
        }

        [LuisIntent("identity-minerals")]
        public async Task PhysicalState(IDialogContext context, LuisResult result)
        {
            mineralTemp = new Mineral();
            localMineralList = new List<Mineral>();

            await context.PostAsync("Qual o estado físico do seu mineral?");
            context.Call(new PhysicalStateDialog(), PhysicalStateResponse);
        }

        private async Task PhysicalStateResponse(IDialogContext context, IAwaitable<string> result)
        {
            string physicalState = await result;

            if (physicalState == "liquido")
            {
                mineralTemp.isLiquid = true;
            }
            else
            {
                mineralTemp.isLiquid = false;
            }

            var requestData = new RequestData()
            {
                mineral = mineralTemp,
                sequence = 1
            };
            List<Mineral> mineralList = await MineralsApi.getByAttibutes(requestData);
            if (mineralList.Count == 1)
            {
                await printMineralResult(mineralList, context);
            }
            else
            {
                localMineralList = mineralList;
                await Brightness(context);
            }
        }

        private async Task Brightness(IDialogContext context)
        {
            await context.PostAsync("Qual o tipo de brilho do seu mineral?");
            context.Call(new BrightnessDialog(), BrightnessResponse);
        }

        private async Task BrightnessResponse(IDialogContext context, IAwaitable<string> result)
        {
            string bright = await result;
            if (bright == "metalico")
            {
                mineralTemp.isMetalic = true;
            }
            else
            {
                mineralTemp.isMetalic = false;
            }
            await Toughness(context);
        }

        private async Task Toughness(IDialogContext context)
        {
            await context.PostAsync("Muito bem. Agora preciso que vc me diga a dureza aproximada do seu mineral.");
            PromptDialog.Text(context, ToughnessResponse, "Lembrando que deve ser um número entre 1 e 10 e que há uma margem de erro de até 0.9 (Mohs).");
        }

        private async Task ToughnessResponse(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;

            ValidateInput validation = new ValidateInput();
            bool valid = validation.ValidaToughness(response);

            if (valid == false)
            {
                await context.PostAsync("Desculpe, mas não consegui identificar esse valor de dureza :-(.");
                PromptDialog.Text(context, ToughnessResponse, "Me informe novamente por favor.");
            }
            else
            {
                mineralTemp.toughness = float.Parse(response, CultureInfo.InvariantCulture.NumberFormat);
                var requestData = new RequestData()
                {
                    mineral = mineralTemp,
                    sequence = 2
                };
                List<Mineral> mineralList = await MineralsApi.getByAttibutes(requestData);

                if (mineralList.Count == 1)
                {
                    await printMineralResult(mineralList, context);
                }
                else if (mineralList.Count == 0)
                {
                    await NotFound(context);
                }
                else
                {
                    localMineralList = mineralList;
                    await Density(context);
                }
            }
        }

        private async Task Density(IDialogContext context)
        {
            await context.PostAsync("Quase lá. Me diga agora qual a densidade do seu mineral.");
            context.Call(new DensityDialog(), DensityResponse);
        }

        private async Task DensityResponse(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;
            var requestData = new RequestData()
            {
                mineral = mineralTemp,
                sequence = 3
            };

            mineralTemp.densityText = response;

            List<Mineral> mineralList = await MineralsApi.getByAttibutes(requestData);

            if (mineralList.Count == 1)
            {
                await printMineralResult(mineralList, context);
            }
            else if (mineralList.Count == 0)
            {
                await NotFound(context);
            }
            else
            {
                localMineralList = mineralList;
                await Color(context, mineralList);
            }
        }

        private async Task Color(IDialogContext context, List<Mineral> mineralList)
        {
            await context.PostAsync("Ok. Preciso agora saber a cor do seu mineral?");
            await Delay();
            context.Call(new ColorDialog(mineralList), ColorResponse);
        }

        private async Task ColorResponse(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;
            mineralTemp.mainColor = response;

            var requestData = new RequestData()
            {
                mineral = mineralTemp,
                sequence = 4
            };
            List<Mineral> mineralList = await MineralsApi.getByAttibutes(requestData);

            if (mineralList.Count == 1)
            {
                await printMineralResult(mineralList, context);
            }
            else if (mineralList.Count == 0)
            {
                await NotFound(context);
            }
            else
            {
                localMineralList = mineralList;
                await TraceColor(context, mineralList);
            }
        }

        private async Task TraceColor(IDialogContext context, List<Mineral> mineralList)
        {
            await context.PostAsync("Muito bem. Me diga qual a cor do traço do seu mineral.");
            context.Call(new TraceColorDialog(mineralList), TraceColorResponse);
        }

        private async Task TraceColorResponse(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;
            mineralTemp.mainTrace = response;

            var requestData = new RequestData()
            {
                mineral = mineralTemp,
                sequence = 5
            };
            List<Mineral> mineralList = await MineralsApi.getByAttibutes(requestData);

            if (mineralList.Count == 1)
            {
                await printMineralResult(mineralList, context);
            }
            else if (mineralList.Count == 0)
            {
                await NotFound(context);
            } else {
                await otherProperties(context, mineralList);
            }
        }

        private async Task otherProperties(IDialogContext context, List<Mineral> mineralList){
            
        }

        public async Task printMineralResult(List<Mineral> minerals, IDialogContext context)
        {
            await context.PostAsync("Pelas suas informações, o possível resultado para o seu mineral é este:");
            await Delay();

            mineralTemp = null;
            localMineralList = null;

            await context.PostAsync(await MineralResult(context, minerals));
            await context.PostAsync(await BackMenu(context));
        }

        public async Task NotFound(IDialogContext context)
        {
            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Menu de opções",
                Value = "menu de opções",
                Type = "imBack"
            });
            var text = "Infelizmente não achei na minha base o seu mineral. Certifique que os dados informados estão corretos.";
            await context.PostAsync(await SuggestAction(context, text, actions));
        }

        private static async Task<IMessageActivity> MineralResult(IDialogContext context, List<Mineral> mineral)
        {
            IMessageActivity menuMessage = context.MakeMessage();
            menuMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            menuMessage.Attachments = await GetAttachment.GetMineralsInformation(mineral);
            return menuMessage;
        }

        private static async Task<IMessageActivity> BackMenu(IDialogContext context)
        {
            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Menu principal",
                Value = "Menu principal",
                Type = "imBack"
            });

            var text = "Abaixo vc pode acessar meu menu de opções.";
            return await SuggestAction(context, text, actions);
        }

        private static async Task<IMessageActivity> Error(IDialogContext context)
        {
            IMessageActivity menuMessage = context.MakeMessage();
            menuMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            menuMessage.Attachments = await GetAttachment.Error();
            return menuMessage;
        }

        private static async Task Delay()
        {
            await Task.Delay(600);
        }

        private static async Task<IMessageActivity> SuggestAction(IDialogContext context, string text, List<CardAction> actions)
        {
            SuggestedActions suggest = new SuggestedActions();
            suggest.Actions = actions;

            var reply = context.MakeMessage();
            reply.Text = text;
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = suggest;
            return reply;
        }
    }
}