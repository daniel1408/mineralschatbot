﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace segchatbot.Dialogs
{
    [Serializable]
    public class PhysicalStateDialog : IDialog<string>
    {
        public async Task StartAsync(IDialogContext context)
        {
            SuggestedActions suggest = new SuggestedActions();

            var actions = new List<CardAction>();
            actions.Add(new CardAction()
            {
                Title = "Líquido",
                Value = "liquido",
                Type = "imBack"
            });

            actions.Add(new CardAction()
            {
                Title = "Sólido",
                Value = "solido",
                Type = "imBack"
            });

            suggest.Actions = actions;
            var reply = context.MakeMessage();

            reply.Text = "Me diga qual o tipo de brilho do seu mineral";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = suggest;

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;
            context.Done(message.Text);
        }
    }
}