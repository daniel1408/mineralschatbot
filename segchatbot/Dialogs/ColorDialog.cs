﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace segchatbot.Dialogs
{
    [Serializable]
    public class ColorDialog : IDialog<string>
    {
        List<Mineral> mineralList;
        public ColorDialog(List<Mineral> mineralList)
        {
            this.mineralList = mineralList;
        }

        public async Task StartAsync(IDialogContext context)
        {
            SuggestedActions suggest = new SuggestedActions();
            var actions = new List<CardAction>();

            List<string> colorTemp = new List<string>();
            foreach (Mineral element in mineralList)
            {
                foreach (Color color in element.colorList)
                {
                    if (colorTemp.IndexOf(color.name) < 0)
                    {
                        actions.Add(new CardAction()
                        {
                            Title = color.name,
                            Value = color.name,
                            Type = "imBack"
                        });
                        colorTemp.Add(color.name);
                    }
                }
            }

            suggest.Actions = actions;
            var reply = context.MakeMessage();

            reply.Text = "Escolha alguma das cores a seguir que melhor representa a cor predominante no seu mineral";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = suggest;

            await context.PostAsync(reply);

            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;
            context.Done(message.Text);
        }
    }
}