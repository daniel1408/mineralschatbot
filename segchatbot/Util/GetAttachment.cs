﻿using BingSearchImageSDK.Services;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace segchatbot.Util
{
    public class GetAttachment
    {
        public async static Task<List<Attachment>> Greeting(string saudacao)
        {
            List<Attachment> listAttachments = new List<Attachment>();
            HeroCard h = new HeroCard
            {
                Images = new List<CardImage>
                {
                    new CardImage
                    {
                        Url = "https://image.ibb.co/eiZ6ue/oie_low.jpg"
                    }
                },
                Title = saudacao,
                Text = "Sou o Tungstênio, seu chatbot mineralogista.",
            };

            listAttachments.Add(h.ToAttachment());
            return listAttachments;
        }

        public async static Task<List<Attachment>> Menu()
        {
            List<Attachment> listAttachments = new List<Attachment>();

            HeroCard a = new HeroCard
            {
                Title = "Indentificar Mineral",
                Text = "Descreva as características do mineral e descubra qual ele é",
                Images = new List<CardImage>
                {
                    new CardImage
                    {
                        Url = "https://image.ibb.co/jhL9N9/search.png"
                    }
                },
                Buttons =
                {
                    new CardAction
                    {
                        Title = "Clique aqui",
                        Value = "identificar algum mineral",
                        Type = "imBack"
                    }
                }
            };

            HeroCard b = new HeroCard
            {
                Title = "Informação de algum mineral",
                Text = "Saiba as informações de algum mineral especifico pelo nome",
                Images = new List<CardImage>
                {
                    new CardImage
                    {
                        Url = "https://image.ibb.co/dGxpN9/information.png"
                    }
                },
                Buttons =
                {
                    new CardAction
                    {
                        Title = "Clique aqui",
                        Value = "saber sobre os minerais",
                        Type = "imBack"
                    }
                }

            };

            HeroCard c = new HeroCard
            {
                Title = "Precisa de Ajuda?",
                Text = "Saiba como eu funciono para melhorar suas explorações.",
                Images = new List<CardImage>
                {
                    new CardImage
                    {
                        Url = "https://image.ibb.co/bCi79p/question.png"
                    }
                },
                Buttons =
                {
                    new CardAction
                    {
                        Title = "Clique aqui",
                        Value = "Preciso de ajuda",
                        Type = "imBack"
                    }
                }
            };

            listAttachments.Add(a.ToAttachment());
            listAttachments.Add(b.ToAttachment());
            listAttachments.Add(c.ToAttachment());
            return listAttachments;
        }

        public async static Task<List<Attachment>> GetMineralsInformation(List<Mineral> m)
        {
            List<Attachment> listAttachments = new List<Attachment>();

            foreach (var item in m)
            {
                string contentUrl = await BingImageSearch.GetContentUrl("pt-BR", item.name);
                if (contentUrl == null)
                {
                    contentUrl = "https://image.freepik.com/vector-gratis/error-de-diseno-404-la-pagina-esta-perdida-y-no-se-encuentra-el-mensaje-plantilla-para-pagina-web-con-error-404-diseno-de-linea-moderna_6280-165.jpg";
                }

                HeroCard a = new HeroCard
                {
                    Images = new List<CardImage> { new CardImage { Url = contentUrl } },
                    Title = item.name + " - " + item.density + " g/cm³",
                    Subtitle = item.description,
                    Text = "Principal cor: " + item.mainColor.ToUpper() + " -  Principal cor do traço: " + item.mainTrace.ToUpper(),
                };

                listAttachments.Add(a.ToAttachment());
            }
            return listAttachments;
        }

        public async static Task<List<Attachment>> GetMineralInformation(Mineral m)
        {
            List<Attachment> listAttachments = new List<Attachment>();
            string contentUrl = await BingImageSearch.GetContentUrl("pt-BR", m.name);
            if (contentUrl == null)
            {
                contentUrl = "https://image.freepik.com/vector-gratis/error-de-diseno-404-la-pagina-esta-perdida-y-no-se-encuentra-el-mensaje-plantilla-para-pagina-web-con-error-404-diseno-de-linea-moderna_6280-165.jpg";
            }

            HeroCard a = new HeroCard
            {
                Images = new List<CardImage> {new CardImage{Url = contentUrl } },
                Title = m.name + " - " + m.density + " g/cm³",
                Subtitle = m.description,
                Text = "Principal cor: " + m.mainColor.ToUpper() + " -  Principal cor do traço: " + m.mainTrace.ToUpper(),
            };
            listAttachments.Add(a.ToAttachment());
            return listAttachments;
        }

        public async static Task<List<Attachment>> Error()
        {
            List<Attachment> listAttachments = new List<Attachment>();
            HeroCard a = new HeroCard
            {
                Title = "Serviço indisponível",
                Subtitle = "Infelizmente não consigo acessar minha base de dados agora, tente novamente mais tarde.",
                Images = new List<CardImage>
                {
                    new CardImage
                    {
                        Url = "https://image.ibb.co/e07LLK/218038_P0_NP5_P_289.jpg"
                    }
                }
            };
            listAttachments.Add(a.ToAttachment());
            return listAttachments;
        }
    }
}