﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace segchatbot.Util
{
    public class Option
    {
        public static List<string> Escolha = new List<string>()
        {
            "Sim",
            "Não"
        };

        public static List<string> Cleavage = new List<string>()
        {
            "Distinta",
            "Indistinta"
        };

        public static List<string> PhysicalState = new List<string>()
        {
            "Sólido",
            "Líquido"
        };
    }
}