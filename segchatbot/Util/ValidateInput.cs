using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace segchatbot.Util
{
    public class ValidateInput
    {
        public bool ValidaToughness(string toughness)
        {

            if (toughness.Where(c => char.IsLetter(c)).Count() > 0)
            {
                return false;
            }
            else
            {
                float toughnessFloat  = float.Parse(toughness, CultureInfo.InvariantCulture.NumberFormat);
                if (toughnessFloat < 0 || toughnessFloat > 10)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public bool ValidaDensity(string density)
        {
            if (density.Where(c => char.IsLetter(c)).Count() > 0)
            {
                return false;
            }
            else
            {
                float densityFloat  = float.Parse(density, CultureInfo.InvariantCulture.NumberFormat);

                if(densityFloat < 0 || densityFloat > 20){
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
}