﻿using Newtonsoft.Json;
using segchatbot.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace segchatbot.Service
{
    public class MineralsApi
    {
        private static string urlDefault = "https://mineralsapi.azurewebsites.net/mineral";
        private static string urlSearch = "https://mineralsapi.azurewebsites.net/identification";

        //private static readonly string urlDefault = "http://localhost:62973/mineral";
        //private static readonly string urlSearch = " http://localhost:62973/identification";

        public static List<Mineral> getAllMinerals()
        {
            try
            {
                string responseData = Request(urlDefault);
                List<Mineral> minerals;
                minerals = JsonConvert.DeserializeObject<List<Mineral>>(responseData);
                return minerals;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static Mineral getMineralById(int id)
        {
            try
            {
                string responseData = Request(urlDefault + "/" + id);
                Mineral mineral;
                mineral = JsonConvert.DeserializeObject<Mineral>(responseData);
                return mineral;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static Mineral getMineralByName(string name)
        {
            string responseData = Request(urlDefault + "/name/" + name);
            Mineral mineral;
            mineral = JsonConvert.DeserializeObject<Mineral>(responseData);
            return mineral;
        }

        public static async Task<List<Mineral>> getByAttibutes(RequestData requestData)
        {
            try
            {
                string endpoint = urlSearch;
                string requestJson = JsonConvert.SerializeObject(requestData);

                HttpContent requestContent = new StringContent(requestJson, Encoding.UTF8, "application/json");
                HttpResponseMessage httpResponse = await MakeHttpPostRequestAsync(requestContent, endpoint);
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new InvalidOperationException("A resposta do serviço não foi bem sucedida.");
                }

                string json = await httpResponse.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(json))
                {
                    return null;
                }
                else
                {
                    List<Mineral> answer = JsonConvert.DeserializeObject<List<Mineral>>(json);
                    return answer;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string Request(string urlRequest)
        {
            try
            {
                string url = string.Format(urlRequest);
                HttpWebRequest quoteRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse quoteResponse = (HttpWebResponse)quoteRequest.GetResponse();
                StreamReader quoteResponseReader = new StreamReader(quoteResponse.GetResponseStream());
                string responseData = quoteResponseReader.ReadToEnd();

                quoteResponseReader.Close();
                quoteRequest.GetResponse().Close();

                return responseData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static async Task<HttpResponseMessage> MakeHttpPostRequestAsync(HttpContent requestContent, string endpoint)
        {
            using (HttpClient client = new HttpClient())
            {
                string token = ConfigurationManager.AppSettings["X-Auth-Token"];
                client.DefaultRequestHeaders.Add("X-Auth-Token", token);
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, endpoint);
                requestMessage.Content = requestContent;
                HttpResponseMessage responseMessage = await client.SendAsync(requestMessage);
                return responseMessage;
            }
        }
    }
}