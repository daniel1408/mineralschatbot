﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace segchatbot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {

        public async Task<HttpResponseMessage> Post([FromBody] Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {
                await Conversation.SendAsync(activity, () => new Dialogo());
            }
            else
            {
                await HandleSystemMessageAsync(activity);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private static async Task PrintErro(Activity activity, string message)
        {
            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            Activity reply = activity.CreateReply(message);
            await connector.Conversations.ReplyToActivityAsync(reply);
        }

        private async Task<Activity> HandleSystemMessageAsync(Activity message)
        {


            if (message.Type == ActivityTypes.Typing)
            {
                var uri = new Uri(message.ServiceUrl);
                var connector = new ConnectorClient(uri);

                Activity isTypingReply = message.CreateReply();
                isTypingReply.Type = ActivityTypes.Typing;

                await connector.Conversations.ReplyToActivityAsync(isTypingReply);
            }

            else if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                var connector = new ConnectorClient(new Uri(message.ServiceUrl));
                Activity isTypingReply = message.CreateReply();
                isTypingReply.Type = ActivityTypes.Typing;
                await connector.Conversations.ReplyToActivityAsync(isTypingReply);
            }

            return null;
        }
    }
}